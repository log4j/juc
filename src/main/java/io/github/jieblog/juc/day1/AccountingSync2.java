package io.github.jieblog.juc.day1;

/**
 * Created by jie on 2017/3/10.
 */
public class AccountingSync2 implements Runnable {
    private static AccountingSync2 instance = new AccountingSync2();
    private static int i = 0;

    @Override
    public void run() {
        for (int j = 0; j < 10000; j++) {
            increase();
        }
    }

    private static synchronized void increase() {
        i++;
    }

    public static void main(String[] args) {
        Thread thread1 = new Thread(instance);
        Thread thread2 = new Thread(instance);

        thread1.start();
        thread2.start();
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(i);
    }
}
