package io.github.jieblog.juc.day1;

/**
 * Created by jie on 2017/3/10.
 */
public class AccountingSyncBad implements Runnable {
    private static int i = 0;

    @Override
    public void run() {
        for (int j = 0; j < 10000; j++) {
            increase();
        }
    }

    public synchronized void increase() {
        i++;
    }

    /**
     * 相对于 AccountingSync 虽然我们对increase()方法 做了同步处理,但是2个线程指向的是不同的实例.换言之就是.
     * 两个线程使用的是两把不同的锁.因此无法保证线程安全
     *
     * @param args
     */
    public static void main(String[] args) {
        Thread thread1 = new Thread(new AccountingSyncBad());
        Thread thread2 = new Thread(new AccountingSyncBad());

        thread1.start();
        thread2.start();
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(i);
    }
}
