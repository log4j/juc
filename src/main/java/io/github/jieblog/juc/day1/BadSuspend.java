package io.github.jieblog.juc.day1;

/**
 * Created by jie on 2017/3/10.
 * <p>
 * 为什么BadSuspend 容易死锁?
 * <p>
 * <p>
 * Thread syspend    不释放锁
 */
public class BadSuspend {
    public static Object u = new Object();
    static ChangeObjectThread t1 = new ChangeObjectThread("t1");
    static ChangeObjectThread t2 = new ChangeObjectThread("t2");

    public static class ChangeObjectThread extends Thread {
        public ChangeObjectThread(String name) {
            super.setName(name);
        }

        @Override
        public void run() {
            synchronized (u) {
                System.out.println("in " + getName());
                Thread.currentThread().suspend();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        t1.start();
        t1.resume();
        //Thread.sleep(100);
        //t2.start();
        //t2.resume();
        t1.join();
        //t2.join();
        System.out.println("thread -> main");
    }
}
