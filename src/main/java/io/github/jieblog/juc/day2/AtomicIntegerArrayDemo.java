package io.github.jieblog.juc.day2;

import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * Created by jie on 2017/3/13.
 */
public class AtomicIntegerArrayDemo {
    private static AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(10);

    public static class Task1 implements Runnable {

        @Override
        public void run() {
            for (int i = 0; i < 10000; i++) {
                //索引位置0-9 1000次 。。。
                atomicIntegerArray.getAndIncrement(i % 10);
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread[] ts = new Thread[10];

        for (int k = 0; k < 10; k++) {
            ts[k] = new Thread(new Task1());
        }

        for (int k = 0; k < 10; k++) {
            ts[k].start();
        }

        for (int k = 0; k < 10; k++) {
            ts[k].join();
        }

        System.out.println(atomicIntegerArray);
    }
}
