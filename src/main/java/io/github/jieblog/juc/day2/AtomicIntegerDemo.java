package io.github.jieblog.juc.day2;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by jie on 2017/3/13.
 */
public class AtomicIntegerDemo {
    private static AtomicInteger atomicInteger = new AtomicInteger(0);

    public static class Task1 implements Runnable {

        @Override
        public void run() {
            System.out.println("当前线程名称->:" + Thread.currentThread().getName());
            for (int i = 0; i < 100000; i++) {
                atomicInteger.incrementAndGet();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Task1 task1 = new Task1();
        Thread[] ts = new Thread[10];

        for (int k = 0; k < 10; k++) {
            ts[k] = new Thread(task1);
        }

        for (int k = 0; k < 10; k++) {
            ts[k].start();
        }

        for (int k = 0; k < 10; k++) {
            ts[k].join();
        }

        System.out.println(atomicInteger.intValue());
    }
}
