package io.github.jieblog.juc.day2;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by jie on 2017/3/13.
 */
public class AtomicReferenceDemo {
    private static AtomicReference<String> stringAtomicReference = new AtomicReference<String>("abc");

    static class Task implements Runnable {

        @Override
        public void run() {
            if (stringAtomicReference.compareAndSet("abc", "qwe")) {
                System.out.println("Thread:" + Thread.currentThread().getId() + " Change value to " + stringAtomicReference);
            } else {
                System.out.println("Thread:" + Thread.currentThread().getId() + " FAILED");
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Task task = new Task();
        Thread[] threads = new Thread[10];

        for (int i = 0; i < 10; i++) {
            threads[i] = new Thread(task);
        }

        for (int i = 0; i < 10; i++) {
            threads[i].start();
            threads[i].join();
        }
    }
}
