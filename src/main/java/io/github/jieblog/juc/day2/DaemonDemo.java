package io.github.jieblog.juc.day2;

/**
 * Created by jie on 2017/3/13.
 * (1) thread.setDaemon(true)必须在thread.start()之前设置，否则会跑出一个IllegalThreadStateException异常。你不能把正在运行的常规线程设置为守护线程。
 * <p>
 * (2) 在Daemon线程中产生的新线程也是Daemon的。
 * <p>
 * (3) 守护线程应该永远不去访问固有资源，如文件、数据库，因为它会在任何时候甚至在一个操作的中间发生中断。
 */
public class DaemonDemo {
    public static void main(String[] args) {
        Task1 task1 = new Task1();
        Thread thread = new Thread(task1);
        thread.setDaemon(true);
        thread.start();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println("主线程唤醒准备结束->");
        }
    }
}

class Task1 implements Runnable {

    @Override
    public void run() {
        System.out.println("daemon task");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println("守护线程唤醒准备执行主线程->");
        }
    }
}