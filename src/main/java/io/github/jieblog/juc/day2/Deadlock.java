package io.github.jieblog.juc.day2;

import sun.applet.Main;

/**
 * Created by jie on 2017/3/13.
 * 模拟死锁
 * <p>
 * 死锁的定义是，两个或两个以上的线程或进程在执行过程中，由于竞争资源或者由于彼此通信而造成的一种阻塞的现象。
 * <p>
 * 是不是觉得定义比较抽象，我们简化一下，就当成只有两个线程。
 * 现在有线程1和线程2。线程1执行过程中，先锁定了对象a，然后需要再锁定b才能继续执行代码；而线程2正巧相反，先锁定了b，需要再锁定a才能继续执行代码。
 * 这时，两个线程都等着对方解锁，才能继续执行，这时，两个线程就进入等待状态，最终不会有线程执行。这就变成了死锁。
 */
public class Deadlock {
    public static void main(String[] args) {
        DeadlockChecker.check();
        Thread t9 = new Thread(
                new DeadLockTask(true));
        Thread t10 = new Thread(
                new DeadLockTask(false));
        t9.start();
        t10.start();
    }
}

class DeadLockTask implements Runnable {

    boolean lockFormer;
    static Object o1 = new Object();
    static Object o2 = new Object();

    DeadLockTask(boolean lockFormer) {
        this.lockFormer = lockFormer;
    }

    @Override
    public void run() {
        if (this.lockFormer) {
            synchronized (o1) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (o2) {
                    System.out.println("1ok");
                }
            }
        } else {
            synchronized (o2) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (o1) {
                    System.out.println("1ok");
                }
            }
        }

    }
}