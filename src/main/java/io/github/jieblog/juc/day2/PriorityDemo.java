package io.github.jieblog.juc.day2;

/**
 * Created by jie on 2017/3/13.
 * <p>
 * 只是一个相对的设置，并不保证，因为线程执行本身就是一个随机的事件。
 */
public class PriorityDemo {
    public static void main(String[] args) {
        HightPriority task1 = new HightPriority();
        LowPriority task2 = new LowPriority();

        Thread thread1 = new Thread(task1);
        Thread thread2 = new Thread(task2);

        thread1.setPriority(Thread.MAX_PRIORITY);
        thread2.setPriority(Thread.MIN_PRIORITY);

        thread1.start();
        thread2.start();
    }
}

/**
 * 优先级较高的线程任务
 */
class HightPriority implements Runnable {

    @Override
    public void run() {
        System.out.println("优先级高->任务开始执行");
    }
}

/**
 * 优先级较低的线程任务
 */
class LowPriority implements Runnable {

    @Override
    public void run() {
        System.out.println("优先级低->任务开始执行");
    }
}