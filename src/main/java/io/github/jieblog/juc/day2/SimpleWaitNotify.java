package io.github.jieblog.juc.day2;

/**
 * Created by jie on 2017/3/13.
 * 容易发生死锁,当 thread2 比 thread1 提前执行。
 * 详见 SimpleWaitNotifyX
 */
public class SimpleWaitNotify {
    final static Object obj = new Object();

    public static void main(String[] args) throws InterruptedException {
        Task1 task1 = new Task1();
        Task2 task2 = new Task2();

        Thread thread1 = new Thread(task1);
        Thread thread2 = new Thread(task2);

        thread1.start();
        thread2.start();
    }

    public static class Task1 implements Runnable {

        @Override
        public void run() {
            System.out.println("task1 ->");
            synchronized (obj) {
                try {
                    obj.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class Task2 implements Runnable {

        @Override
        public void run() {
            System.out.println("task2 ->");
            synchronized (obj) {
                obj.notify();
            }
        }
    }

}

