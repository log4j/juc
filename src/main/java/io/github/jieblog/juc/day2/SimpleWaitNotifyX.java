package io.github.jieblog.juc.day2;

/**
 * Created by jie on 2017/3/13.
 * thread1 比 thread2 提前执行。
 */
public class SimpleWaitNotifyX {
    final static Object obj = new Object();

    public static void main(String[] args) throws InterruptedException {
        SimpleWaitNotify.Task1 task1 = new SimpleWaitNotify.Task1();
        SimpleWaitNotify.Task2 task2 = new SimpleWaitNotify.Task2();

        Thread thread1 = new Thread(task1);
        Thread thread2 = new Thread(task2);

        thread2.start();
        Thread.sleep(1000);
        thread1.start();
    }

    public static class Task1 implements Runnable {

        @Override
        public void run() {
            System.out.println("task1 ->");
            synchronized (obj) {
                try {
                    obj.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class Task2 implements Runnable {

        @Override
        public void run() {
            System.out.println("task2 ->");
            synchronized (obj) {
                obj.notify();
            }
        }
    }

}
