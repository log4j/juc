package io.github.jieblog.juc.day4.simplepool;

/**
 * Created by jie on 2017/3/17.
 */
public class SimpleThreadPoolDemo {
    public static void main(String[] args) {
        long starttime = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            ThreadPool.getInstance().start(new MyTask("testThreadPool" + Integer.toString(i)));
        }

        long endtime = System.currentTimeMillis();
        System.out.println("testThreadPool" + ": " + (endtime - starttime));
        System.out.println("getCreatedThreadsCount:" + ThreadPool.getInstance().getCreatedThreadsCount());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class MyTask implements Runnable {
    protected String name;

    public MyTask() {
    }

    public MyTask(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println(name);
    }
}