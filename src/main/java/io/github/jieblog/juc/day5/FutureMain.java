package io.github.jieblog.juc.day5;

import java.util.concurrent.*;

/**
 * Created by jie on 2017/3/18.
 * JDK 的异步使用
 */
public class FutureMain {
    public static void main(String[] args) throws Exception {
        FutureTask<String> futureTask = new FutureTask<String>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                //模拟执行异步任务
                Thread.sleep(3000);
                return "async result";
            }
        });

        ExecutorService executor = Executors.newFixedThreadPool(1);
        executor.submit(futureTask);

        System.out.println("请求完毕");

        //模拟继续执行主线任务
        Thread.sleep(1000);

        System.out.println(futureTask.get());
    }
}
