package io.github.jieblog.juc.day7;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by jie on 2017/6/16.
 */
public class BlockingQueueLTest {
    private static BlockingQueue<String> blockingQueue = new LinkedBlockingQueue<>();

    public static void addElement(String ele) {
        blockingQueue.add(ele);
    }

    public static String getElement() {
        String result = null;
        try {
            result = blockingQueue.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }
}
