package io.github.jieblog.juc.day7;

import java.util.Date;

/**
 * Created by jie on 2017/6/16.
 */
public class GetMessage {
    public static void main(String[] args) {
        String message = BlockingQueueLTest.getElement();
        System.out.println(message + new Date());
    }
}
