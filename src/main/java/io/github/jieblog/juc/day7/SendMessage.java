package io.github.jieblog.juc.day7;

import java.util.Date;
import java.util.concurrent.*;

/**
 * Created by jie on 2017/6/16.
 */
public class SendMessage {
    public static void main(String[] args) {




        FutureTask task = new FutureTask(new Callable() {
            @Override
            public Object call() throws Exception {
                Thread.sleep(2000);
                for (int i = 0; i < 10; i++) {
                    BlockingQueueLTest.addElement("wangjie -->" + i);
                }
                return null;
            }
        });
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(task);
        System.out.println(new Date().getTime());
        String message = BlockingQueueLTest.getElement();
        System.out.println(message + new Date().getTime());

    }
}
