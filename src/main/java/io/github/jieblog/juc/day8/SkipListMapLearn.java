package io.github.jieblog.juc.day8;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Created by jie on 2017/6/17.
 */
public class SkipListMapLearn {
    public static void main(String[] args) {
        Map tree = new TreeMap();
        Map linked = new LinkedHashMap();
        Map hash = new HashMap();
        Map skipList = new ConcurrentSkipListMap();
        System.out.println("tree :" + buildMap(tree));
        System.out.println("link :" + buildMap(linked));
        System.out.println("hash :" + buildMap(hash));
        System.out.println("skipListMap :" + buildMap(skipList));
    }

    private static Map buildMap(Map map) {
        map.put("0", "a");
        map.put("e", "b");
        map.put("4", "s");
        map.put("3", "c");
        return map;
    }
}
