package io.github.jieblog.juc.day9;

/**
 * jie
 * 2017-06-2017/6/21
 * 懒汉设计模式
 */
public class LazySingleton {
    public static String num = "1";
    private static LazySingleton singleton;

    private LazySingleton() {
        System.out.println("懒汉被初始化");
    }

    public static LazySingleton getInstance() {
        if (singleton == null) {
            singleton = new LazySingleton();
        }
        return singleton;
    }
}
