package io.github.jieblog.juc.day9;

/**
 * jie
 * 2017-06-2017/6/21
 * 饱汉设计模式的单例
 */
public class Singleton {
    /**
     * 对singleton字段或方法的引用，都回到导致该类被初始化，
     * 但是类初始化只有一次，因此instance 实例永远只会被创建一次
     */
    public static String num = "1";

    private Singleton() {
        System.out.println("饱汉被初始化");
    }

    private static Singleton singleton = new Singleton();

    public Singleton getInstance() {
        return singleton;
    }
}
