package io.github.jieblog.juc.day9;

/**
 * jie
 * 2017-06-2017/6/21
 */
public class SingletonInitTest {
    public static void main(String[] args) {
        /**
         * 对singleton字段或方法的引用，都回到导致该类被初始化，
         * 但是类初始化只有一次，因此instance 实例永远只会被创建一次
         */
        System.out.println(Singleton.num);
        /**
         * 懒汉就不会被初始化。。。滴滴滴
         */
        System.out.println(LazySingleton.num);
    }
}
